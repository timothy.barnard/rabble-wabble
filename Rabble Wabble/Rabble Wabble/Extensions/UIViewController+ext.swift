//
//  UIViewController+ext.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 09/09/2021.
//

import UIKit

protocol ViewController {
    static var storyboardName: String {get}
    static var identifier: String {get}
}

extension ViewController where Self: UIViewController {
    
    /// Helper method to create a view controller
    /// - Returns: `UIViewController` optional
    static func createViewController() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
}
