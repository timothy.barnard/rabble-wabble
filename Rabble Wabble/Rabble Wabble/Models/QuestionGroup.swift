//
//  QuestionGroup.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import Foundation
import Combine

//Wrapper the questions in groups
public class QuestionGroup: Codable {
    
    public class Score: Codable {
        public var correctCount: Int = 0 {
            didSet {
                updateRunningPercentage()
            }
        }
        public var incorrectCount: Int = 0 {
            didSet {
                updateRunningPercentage()
            }
        }
        
        @Published public var runningPercentage: Double = 0
        
        private enum CodingKeys: String, CodingKey {
            case correctCount
            case incorrectCount
        }
        
        public init () {}
        
        public required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.correctCount = try container.decode(Int.self, forKey: .correctCount)
            self.incorrectCount = try container.decode(Int.self, forKey: .incorrectCount)
            updateRunningPercentage()
        }
        
        private func updateRunningPercentage() {
            let totalCount = correctCount + incorrectCount
            guard totalCount > 0 else {
                runningPercentage = 0
                return
            }
            runningPercentage = Double(correctCount) / Double(totalCount)
        }
        
        public func reset() {
            self.correctCount = 0
            self.incorrectCount = 0
        }
    }
    
    public let questions: [Question]
    public let title: String
    public private(set) var score: Score
    
    public init(questions: [Question], title: String, score: Score = .init()) {
        self.questions = questions
        self.title = title
        self.score = score
    }
}
