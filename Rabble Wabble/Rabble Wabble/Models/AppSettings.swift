//
//  AppSettings.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import Foundation

class AppSettings {
    
    //MARK: - Keys
    private enum Keys: String {
        case questionStrategy = "questionStrategy"
    }
    
    public static let shared: AppSettings = .init()
    
    //MARK: - Instance properties
    public var questionStrategyType: QuestionStrategyType {
        get {
            let rawValue = userDefaults.integer(forKey: Keys.questionStrategy.rawValue)
            return QuestionStrategyType(rawValue: rawValue) ?? .sequential
        }
        set {
            userDefaults.set(newValue.rawValue, forKey: Keys.questionStrategy.rawValue)
        }
    }
    
    private let userDefaults: UserDefaults = .standard

    
    //MARK: - Object Lifecycle
    private init() {}
    
    //MARK: - Instance methods
    public func questionStrategy(
        for questionGroupCaretaker: QuestionGroupCaretaker
    ) -> QuestionStrategy {
        questionStrategyType.strategyGroup(for: questionGroupCaretaker)
    }
}


public enum QuestionStrategyType: Int, CaseIterable {
    
    case random
    case sequential
    
    /// Get the title of the given case
    /// - Returns: `String`
    public func title() -> String {
        switch self {
        case .random: return "Random"
        case .sequential: return "Sequential"
        }
    }
    
    /// Gets the strategy group from the given case
    /// - Parameter questionGroupCaretaker: `QuestionGroupCaretaker`
    /// - Returns: `QuestionStrategy`
    func strategyGroup(
        for questionGroupCaretaker: QuestionGroupCaretaker
    ) -> QuestionStrategy {
        switch self {
        case .random: return RandomQuestionStrategy(questionGroupCaretaker: questionGroupCaretaker)
        case .sequential: return SequentialQuestionStrategy(questionGroupCaretaker: questionGroupCaretaker)
        }
    }
}
