//
//  AppSettingsViewController.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import UIKit

class AppSettingsViewController: UITableViewController {
    
    //MARK: - Properties
    public let appSettings: AppSettings = .shared
    private let identifier = "appSettingsCell"
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
    }
}

//MARK: - UITableViewDataDelegate
extension AppSettingsViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = QuestionStrategyType.allCases[indexPath.row]
        appSettings.questionStrategyType = type
        tableView.reloadData()
    }
}

//MARK: - UITableViewDataSource
extension AppSettingsViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return QuestionStrategyType.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        let type = QuestionStrategyType.allCases[indexPath.row]
        cell.textLabel?.text = type.title()
        
        if appSettings.questionStrategyType == type {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
}
