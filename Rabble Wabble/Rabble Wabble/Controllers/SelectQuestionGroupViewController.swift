//
//  SelectQuestionGroupViewController.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import UIKit

class SelectQuestionGroupViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet public var tableView: UITableView! {
        didSet {
            tableView.register(.init(nibName: QuestionGroupCell.name, bundle: nil), forCellReuseIdentifier: QuestionGroupCell.identifier)
            tableView.tableFooterView = UIView()
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    //MARK: - Properties
    private var questionGroupCaretaker: QuestionGroupCaretaker = .init()
    
    public var questionGroups: [QuestionGroup] {
        return questionGroupCaretaker.questionGroups
    }
    
    private var selectedQuestionGroup: QuestionGroup! {
        get { return questionGroupCaretaker.selectedQuestionGroup }
        set { questionGroupCaretaker.selectedQuestionGroup = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionGroups.forEach {
            print("\($0.title): correctCount: \($0.score.correctCount), incorrectCount: \($0.score.incorrectCount)")
        }
    }
    
    //MARK: Actions
    @IBAction func settingsButtonPressed(_ sender: UIBarButtonItem) {
        let vc = AppSettingsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        let navigationController = CreateQuestionGroupViewController.createViewController() as! UINavigationController
        let vc = navigationController.topViewController as! CreateQuestionGroupViewController
        vc.delegate = self
        self.present(navigationController, animated: true)
    }
}


//MARK: - UITableViewDataSource
extension SelectQuestionGroupViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionGroups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: QuestionGroupCell.identifier) as? QuestionGroupCell else {
            fatalError("Unable to dequeueReusableCell of type QuestionGroupCell")
        }
        let questionGroup = self.questionGroups[indexPath.row]
        cell.setupCell(questionGroup)
        return cell
    }
}

//MARK: - UITableViewDelegate
extension SelectQuestionGroupViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let group = self.questionGroups[indexPath.row]
        questionGroupCaretaker.selectedQuestionGroup = group
        let vc = QuestionViewController.createViewController() as! QuestionViewController
        vc.questionStrategy = AppSettings.shared.questionStrategy(for: questionGroupCaretaker)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK: - QuestionViewControllerDelegate
extension SelectQuestionGroupViewController: QuestionViewControllerDelegate {
    func questionViewController(_ viewController: QuestionViewController, didCancel questionStrategy: QuestionStrategy) {
        navigationController?.popToViewController(self, animated: true)
    }
    
    func questionViewController(_ viewController: QuestionViewController, didComplete questionStrategy: QuestionStrategy) {
        navigationController?.popToViewController(self, animated: true)
    }
}

//MARK: - CreateQuestionGroupViewControllerDelegate
extension SelectQuestionGroupViewController: CreateQuestionGroupViewControllerDelegate {
    
    func createQuestionGroupViewControllerDidCancel(_ viewController: CreateQuestionGroupViewController) {
        dismiss(animated: true)
    }
    
    func createQuestionGroupViewController(_ viewController: CreateQuestionGroupViewController, created questionGroup: QuestionGroup) {
        questionGroupCaretaker.questionGroups.append(questionGroup)
        try? questionGroupCaretaker.save()
        dismiss(animated: true)
        tableView.reloadData()
    }
}
