//
//  ViewController.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import UIKit

protocol QuestionViewControllerDelegate: AnyObject {
    func questionViewController(
        _ viewController: QuestionViewController,
        didCancel questionStrategy: QuestionStrategy)
    func questionViewController(
        _ viewController: QuestionViewController,
        didComplete questionStrategy: QuestionStrategy)
}


class QuestionViewController: UIViewController, ViewController {
    
    static var identifier = "QuestionViewController"
    static var storyboardName: String = "Main"
    
    // MARK: - Instance Properties
    public var questionStrategy: QuestionStrategy! {
        didSet {
            navigationItem.title = questionStrategy.title
        }
    }
    public weak var delegate: QuestionViewControllerDelegate?
    
    public var questionIndex = 0
    
    public var correctCount = 0
    public var incorrectCount = 0
    
    /// Controller View
    public var questionView: QuestionView! {
        guard isViewLoaded else { return nil }
        return (view as! QuestionView)
    }
    
    //MARK: - Private properties
    private lazy var questionIndexItem: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        item.tintColor = .black
        navigationItem.rightBarButtonItem = item
        return item
    }()
    
    
    // MARK: - View Lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        showQuestion()
        setupCancelButton()
    }
    
    /**
    Shows a question from the current question index.
     1. Picks the current index question
     2. Sets the labels with the question data
     3. Hides the answer and hint labels
     */
    private func showQuestion() {
        let question = questionStrategy.currentQuestion()
        
        questionView.answerLabel.text = question.answer
        questionView.promptLabel.text = question.prompt
        questionView.hintLabel.text = question.hint
        
        questionView.answerLabel.isHidden = true
        questionView.hintLabel.isHidden = true
        questionIndexItem.title = questionStrategy.questionIndexTitle()
    }
    
    /// Shows/setups the cancel button on the navigation bar
    private func setupCancelButton() {
        let action = #selector(cancelButtonPressed(_:))
        let image = UIImage(named: "ic_menu")
        navigationItem.leftBarButtonItem = .init(
            image: image, style: .plain, target: self, action: action)
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    
    @objc func cancelButtonPressed(_ sender: UIBarButtonItem) {
        delegate?.questionViewController(self, didCancel: questionStrategy)
    }
    
    // MARK: - Actions
    @IBAction func toggleAnswerLabels(_ sender: Any) {
        questionView.answerLabel.isHidden.toggle()
        questionView.hintLabel.isHidden.toggle()
    }
    
    @IBAction func handleCorrect(_ sender: Any) {
        let question = questionStrategy.currentQuestion()
        questionStrategy.markQuestionCorrect(question)
        showNextQuestion()
    }

    @IBAction func handleIncorrect(_ sender: Any) {
        let question = questionStrategy.currentQuestion()
        questionStrategy.markQuestionIncorrect(question)
        showNextQuestion()
    }

    private func showNextQuestion() {
        if questionStrategy.advanceToNextQuestion() {
            showQuestion()
        } else {
            delegate?.questionViewController(self, didComplete: questionStrategy)
        }
    }
}

