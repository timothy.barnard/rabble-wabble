//
//  QuestionGroupCell.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import UIKit
import Combine

class QuestionGroupCell: UITableViewCell {
    
    public static let name = "QuestionGroupCell"
    public static let identifier = "QuestionGroupCell"
    public var percentageSubscriber: AnyCancellable?

    @IBOutlet public var titleLabel: UILabel!
    @IBOutlet public var percentageLabel: UILabel!
    
    /// Helper method for setting up the cell with the given `QuestionGroup`
    /// - Parameter questionGroup: `QuestionGroup`
    func setupCell(_ questionGroup: QuestionGroup) {
        self.titleLabel.text = questionGroup.title
        percentageSubscriber = questionGroup.score.$runningPercentage
            .receive(on: DispatchQueue.main)
            .map {
                String(format: "%.0f %%", round(100 * $0))
            }
            .assign(to: \.text, on: percentageLabel)
    }
    
}
