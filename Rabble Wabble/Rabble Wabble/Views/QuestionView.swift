//
//  QuestionView.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import UIKit

class QuestionView: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet public var answerLabel: UILabel!
    @IBOutlet public var correctCountLabel: UILabel!
    @IBOutlet public var incorrectCountLabel: UILabel!
    @IBOutlet public var promptLabel: UILabel!
    @IBOutlet public var hintLabel: UILabel!
}
