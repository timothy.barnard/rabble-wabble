//
//  QuestionGroupBuilder.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation

public class QuestionGroupBuilder {
    
    public enum Error: Swift.Error {
        case missingTitle
        case missingQuestions
    }
    
    //MARK: - Public properties
    public var questions: [QuestionBuilder] = .init()
    public var title: String?
    
    //MARK: - Public methods
    
    /// Adds a new question
    public func addNewQuestion() {
        let question = QuestionBuilder()
        self.questions.append(question)
    }
    
    /// Removes a question from the given index
    /// - Parameter index: `Int`
    public func removeQuestion(at index: Int) {
        self.questions.remove(at: index)
    }
    
    /// Builds the question group
    /// - Throws: `Error` if something went wrong
    /// - Returns: `QuestionGroup`
    public func build() throws -> QuestionGroup {
        guard let title = self.title, !title.isEmpty else {
            throw Error.missingTitle
        }
        
        guard self.questions.count > 0 else {
            throw Error.missingQuestions
        }
        
        let questions = try self.questions.map { try $0.build() }
        return QuestionGroup(questions: questions, title: title)
    }
}
