//
//  QuestionBuilder.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation

public class QuestionBuilder {
    
    public enum Error: Swift.Error {
        case missingAnswer
        case missingPrompt
    }
    
    //MARK: - Public
    public var answer: String?
    public var hint: String?
    public var prompt: String?
    
    /// Builds the question
    /// - Throws: `Error` if something went wrong
    /// - Returns: `Question`
    public func build() throws -> Question {
        guard let answer = self.answer, !answer.isEmpty else {
            throw Error.missingAnswer
        }
        guard let prompt = self.prompt, !prompt.isEmpty else {
            throw Error.missingPrompt
        }
        return .init(answer: answer, hint: hint ?? "", prompt: prompt)
    }
}
