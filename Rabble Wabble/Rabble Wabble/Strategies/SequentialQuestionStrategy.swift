//
//  SequentialQuestionStrategy.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import Foundation

public class SequentialQuestionStrategy: BaseQuestionStrategy {
    
    convenience init(questionGroupCaretaker: QuestionGroupCaretaker) {
        let questionGroup = questionGroupCaretaker.selectedQuestionGroup!
        let questions = questionGroup.questions
        self.init(questionGroupCaretaker: questionGroupCaretaker, questions: questions)
    }
}
