//
//  QuestionStrategy.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import Foundation

public protocol QuestionStrategy: AnyObject {
    
    //MARK: - Properties
    
    //Title of the question
    var title: String {get}
    var correctCount: Int {get}
    var incorrectCount: Int {get}
    
    //MARK: - Methods
    
    /**
     Will advance to the next question.
     If the next question is not available, then it will return false
     otherwise true.
     */
    func advanceToNextQuestion() -> Bool
    func currentQuestion() -> Question
    func markQuestionCorrect(_ question: Question)
    func markQuestionIncorrect(_ question: Question)
    
    //Gets the current index title: 1/10
    func questionIndexTitle() -> String
}
