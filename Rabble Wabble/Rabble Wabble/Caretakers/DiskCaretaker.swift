//
//  DiskCaretaker.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import Foundation

public final class DiskCaretaker {
    
    private static let decoder: JSONDecoder = .init()
    private static let encoder: JSONEncoder = .init()
    
    public static func createDocumentURL(with name: String) -> URL {
        let fileManager = FileManager.default
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        return url.appendingPathComponent(name).appendingPathExtension("json")
    }
    
    /// Save the object to json file with the given file name
    /// - Parameters:
    ///   - object: `T: Codable` the object to store
    ///   - fileName: `String` the name of the file
    /// - Throws: `Error`
    public static func save<T: Codable>(_ object: T, to fileName: String) throws {
        let url = self.createDocumentURL(with: fileName)
        let data = try encoder.encode(object)
        try data.write(to: url, options: .atomic)
    }
    
    /// Retrieve and decode the data from the given file name
    /// - Parameters:
    ///   - type: `T: Codable` the object to decode into
    ///   - fileName: `String` the name of the file
    /// - Throws: `Error`
    /// - Returns: `T: Codable`
    public static func retrieve<T: Codable>(_ type: T.Type, from fileName: String) throws -> T {
        let url = self.createDocumentURL(with: fileName)
        return try retrieve(T.self, from: url)
    }
    
    /// Retrieve and decode the data from the given file
    /// - Parameters:
    ///   - type: `T: Codable` the object to decode into
    ///   - fileName: `URL` the file URL
    /// - Throws: `Error`
    /// - Returns: `T: Codable`
    public static func retrieve<T: Codable>(_ type: T.Type, from url: URL) throws -> T {
        let data = try Data(contentsOf: url)
        return try decoder.decode(T.self, from: data)
    }
}
