//
//  QuestionGroupCaretaker.swift
//  Rabble Wabble
//
//  Created by Timothy Barnard on 08/09/2021.
//

import Foundation

class QuestionGroupCaretaker {
    
    //MARK: - Properties
    private let fileName: String = "QuestionGroupData"
    
    public var questionGroups: [QuestionGroup] = []
    public var selectedQuestionGroup: QuestionGroup!
    
    //MARK: - Lifecycle
    public init() {
        loadQuestionGroups()
    }
    
    private func loadQuestionGroups() {
        if let groups = try? DiskCaretaker.retrieve([QuestionGroup].self, from: fileName) {
            self.questionGroups = groups
        } else {
            let url = Bundle.main.url(forResource: fileName, withExtension: "json")!
            self.questionGroups = try! DiskCaretaker.retrieve([QuestionGroup].self, from: url)
            try? save()
        }
    }
    
    public func save() throws {
        try DiskCaretaker.save(questionGroups, to: fileName)
    }
}
